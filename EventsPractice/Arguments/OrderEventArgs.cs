﻿using EventsPractice.Models;

namespace EventsPractice.Arguments
{
    public class OrderEventArgs : EventArgs
    {
        public Order Order { get; set; }

        public OrderEventArgs(Order order)
        {
            Order = order;
        }
    }
}
