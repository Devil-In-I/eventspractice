﻿using EventsPractice.Models;

namespace EventsPractice.Arguments
{
    public class WaiterEventArgs : EventArgs
    {
        public string DishName { get; set; }
        public WaiterEventArgs(string dishName)
        {
            DishName = dishName;
        }
    }
}
