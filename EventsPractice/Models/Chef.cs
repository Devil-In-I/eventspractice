﻿using EventsPractice.Arguments;
using EventsPractice.Utilities;

namespace EventsPractice.Models
{
    public delegate void DishCookedEventHandler(Object sender, OrderEventArgs e);
    public class Chef : BaseEntity
    {
        public string Name { get; set; }

        public event DishCookedEventHandler? OnDishCooked;

        public Chef()
        {
            Id = new Random().Next(1, 20);
            Name = "Gordon Ramsay";
        }

        public Chef(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public void OrderRecievedHandler(object sender, OrderEventArgs e)
        {
            ResetCounter();
            ColoredPrint.Print($"Chef {Id} - {Name}, just recieved an order #{e.Order.Id}.", ConsoleColor.Magenta);
            ColoredPrint.Print($"Started cooking {e.Order.DishName} at {DateTime.UtcNow}.", ConsoleColor.Magenta);
            Thread.Sleep(TimeSpan.FromSeconds(5));
            ColoredPrint.Print($"{e.Order.DishName} is done by {DateTime.UtcNow}.", ConsoleColor.Magenta);
            OnDishCooked?.Invoke(this, e);
        }

        private void ResetCounter()
        {
            Interlocked.Exchange(ref Waiter.OnDishCookedEventCounter, 0);
        }
    }
}
