﻿using EventsPractice.Utilities;

namespace EventsPractice.Models
{
    public class Order : BaseEntity
    {
        public string DishName { get; set; }
        public int TableNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public Order(int id, string dishName, int tableNumber)
        {
            Id = id;
            DishName = dishName;
            TableNumber = tableNumber;
            CreatedDate = DateTime.UtcNow;

            ColoredPrint.Print($"Order #{id} just has been created: {CreatedDate}", ConsoleColor.Green);
        }
    }
}
