﻿using EventsPractice.Arguments;
using EventsPractice.Utilities;

namespace EventsPractice.Models
{
    public delegate void OrderRecievedEventHandler(Object sender, OrderEventArgs e);
    public class OrderManager : BaseEntity
    {
        public event OrderRecievedEventHandler? OnOrderRecieved;

        public OrderManager(int id)
        {
            Id = id;
            ColoredPrint.Print($"Manager #{id} started working.", ConsoleColor.DarkBlue);
        }

        public void RecieveOrder(Order order)
        {
            var args = new OrderEventArgs(order);
            ColoredPrint.Print($"Manager {Id} receieved an order #{order.Id} at {DateTime.UtcNow}", ConsoleColor.Blue);
            Thread.Sleep(TimeSpan.FromSeconds(1.5));
            OnOrderRecieved?.Invoke(this, args);
        }
    }
}
