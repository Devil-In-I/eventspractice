﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsPractice.Models
{
    public static class Restaurant
    {
        public static void ServeClients()
        {
            var manager = new OrderManager(1);
            var chef = new Chef(1, "Abylaihan");

            manager.OnOrderRecieved += chef.OrderRecievedHandler;

            var dishes = new List<string>
            {
                "Plov",
                "Shashlyk",
                "Fried rice",
                "Besparmak",
                "Kazy"
            };
            var waiters = new List<Waiter>
            {
                new Waiter(1, "Zhibek"),
                new Waiter(2, "Evgeniy"),
                new Waiter(3, "Roman"),
                new Waiter(4, "Volodya")
            };

            foreach (var waiter in waiters)
            {
                chef.OnDishCooked += waiter.OnDishCookedEventHandler;
            }

            for (int i = 0; i < 3; i++)
            {
                var tableNum = i * new Random().Next(1, 15);
                var dishName = dishes[new Random().Next(0, dishes.Count)];
                manager.RecieveOrder(new Order(i, dishName, tableNum));

                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
