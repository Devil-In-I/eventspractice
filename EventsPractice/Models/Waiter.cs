﻿using EventsPractice.Arguments;
using EventsPractice.Utilities;
using System.Diagnostics.Tracing;

namespace EventsPractice.Models
{
    public class Waiter : BaseEntity
    {
        public static int OnDishCookedEventCounter = 0;

        public string Name { get; set; }

        public Waiter(int id, string name)
        {
            Name = name;
            Id = id;
        }

        public void OnDishCookedEventHandler(object sender, OrderEventArgs e)
        {
            if (Interlocked.CompareExchange(ref OnDishCookedEventCounter, 1, 0) == 0)
            {
                ColoredPrint.Print($"Waiter #{Id} took the order {e.Order.Id} at {DateTime.UtcNow}.", ConsoleColor.Yellow);
                ColoredPrint.Print($"The waiter is now carrying the order to table number {e.Order.TableNumber}.", ConsoleColor.Yellow);
                Thread.Sleep(TimeSpan.FromSeconds(1));
                ColoredPrint.Print($"Waiter #{Id} has delivered the order to table number {e.Order.TableNumber}.", ConsoleColor.Yellow);
            }
        }
    }
}
