﻿using System.Drawing;

namespace EventsPractice.Utilities
{
    public static class ColoredPrint
    {
        public static void Print(string output, ConsoleColor textColor)
        {
            Console.ForegroundColor = textColor;
            Console.WriteLine(output);
            Console.ForegroundColor = default;
        }
    }
}
