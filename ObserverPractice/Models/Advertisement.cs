﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPractice.Models
{
    public class Advertisement
    {
        public string Subject { get; set; }
        public string Description { get; set; }
    }
}
