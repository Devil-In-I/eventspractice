﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPractice.Models
{
    public class Customer : IObserver<Advertisement>
    {
        private IDisposable unsubscriber;

        public string Name { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }

        public Customer(string name, string email = "example@mail.ru", string location = "Karagandy")
        {
            Name = name;
            Email = email;
            Location = location;
        }

        public virtual void Subscribe(IObservable<Advertisement> provider)
        {
            if (provider != null)
            {
                unsubscriber = provider.Subscribe(this);
            }
        }

        public void OnCompleted()
        {
            // No sense of using this method
        }

        public void OnError(Exception error)
        {
            Console.WriteLine($"Customer {Name} couldn't get the advertisement");
            Console.WriteLine($"{error.Source}");
        }

        public void OnNext(Advertisement ad)
        {
            Console.WriteLine($"{Name} just recieved arrival info about a brand new {ad.Subject}.");
            Console.WriteLine($"{ad.Subject.ToUpperInvariant()} is described as «{ad.Description}»");
        }

        public virtual void Unsubsribe()
        {
            unsubscriber.Dispose();
            Console.WriteLine($"{Name} no longer wants to recieve notifications.");
        }
    }
}
