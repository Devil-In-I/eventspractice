﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPractice.Models
{
    public class Store : IObservable<Advertisement>
    {
        private HashSet<IObserver<Advertisement>> observers;

        public string Name { get; set; }
        /// <summary>
        /// Type of store(e.g. sports, geek, groccery etc.)
        /// </summary>
        public string StoreType { get; set; }

        public Advertisement Avertisement { get; set; }

        public Store(string name, string type)
        {
            Name = name;
            StoreType = type;
            observers = new HashSet<IObserver<Advertisement>>();
        }

        public IDisposable Subscribe(IObserver<Advertisement> observer)
        {
            observers.Add(observer);
            return new Unsubscriber(observers,observer);
        }

        private sealed class Unsubscriber : IDisposable
        {
            private ISet<IObserver<Advertisement>> observers;
            private IObserver<Advertisement> observer;

            public Unsubscriber(ISet<IObserver<Advertisement>> _observers, IObserver<Advertisement> _observer)
            {
                observers = _observers;
                observer = _observer;
            }

            public void Dispose()
            {
                if (observer != null)
                {
                    observers.Remove(observer);
                }
            }
        }

        public void GetNewProductStuff(Advertisement advertisement)
        {

            foreach(var observer in observers)
            {
                if (advertisement != null)
                {
                    observer.OnNext(advertisement);
                }
                else
                {
                    observer.OnError(new ArgumentNullException(nameof(advertisement), "Unpredictable failure has just happend."));
                }
            }
        }
    }
}
