﻿using ObserverPractice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPractice
{
    public class Simulation
    {
        public static void Simulate()
        {
            var store1 = new Store("Marwin", "Geek");
            var store2 = new Store("Technodom", "Electronics");

            var customer1 = new Customer("Nikita");
            var customer2 = new Customer("Dmitriy");
            var customer3 = new Customer("Alexander");
            var customer4 = new Customer("Iliya");

            customer1.Subscribe(store1);
            customer2.Subscribe(store2);

            customer3.Subscribe(store2);
            customer4.Subscribe(store1);

            store1.GetNewProductStuff(new Advertisement
            {
                Subject = "Amazing Spider-Man Omnibus, Vol. 1",
                Description = @"Meet the brand new spider-man comics"
            });
            store2.GetNewProductStuff(new Advertisement
            {
                Subject = "RTX 4090TI Quadro Turbo Ultra Giga Ubermensch",
                Description = "You don't need all that trash that claims to be a graphics card." +
                              " All you need is RTX 4090TI. Up to 5 days since the notification you can buy one with 5% discount"
            });
        }
    }
}
