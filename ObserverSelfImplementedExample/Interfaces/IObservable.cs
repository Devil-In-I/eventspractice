﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Interfaces
{
    public interface IObservable
    {
        void Subscribe(IObserver viewer);
        void Unsubscribe(IObserver viewer);
    }
}
