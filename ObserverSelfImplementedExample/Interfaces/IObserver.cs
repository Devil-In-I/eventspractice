﻿using ObserverSelfImplementedExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Interfaces
{
    public interface IObserver
    {
        void Notify(NotificationData data);
        string ToString();
    }
}
