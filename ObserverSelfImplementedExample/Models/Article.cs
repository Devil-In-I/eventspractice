﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public Article(string title, string content)
        {
            Id = new Random().Next(1, 100);
            Title = title;
            Content = content;
        }
    }
}
