﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Blog? Blog { get; private set; }

        public Author(string name)
        {
            Id = new Random().Next(1, 100);
            Name = name;
        }

        public Blog CreateBlog(string name)
        {
            Blog = new Blog(name, this);
            return Blog;
        }

        public void CreateNewArticle(Article article)
        {
            Blog?.AddArticle(article);
        }
    }
}
