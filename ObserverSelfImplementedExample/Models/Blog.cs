﻿using EventsPractice.Utilities;
using ObserverSelfImplementedExample.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Models
{
    public class Blog : IObservable
    {
        private readonly ISet<IObserver> viewers;

        public Author Author { get; private set; }
        public List<Article> Articles { get; private set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int ViewersCount { get; private set; }

        public Blog(string name, Author author)
        {
            Id = new Random().Next(1, 100);
            viewers = new HashSet<IObserver>();
            Name = name ?? "Richard's notes";
            ViewersCount = 0;
            Author = author;
            Articles = new List<Article>();
        }

        public void Subscribe(IObserver viewer)
        {
            viewers.Add(viewer);
            ViewersCount++;
            ColoredPrint.Print($"{viewer.ToString()} has subscribed to  our «{Name}» blog", ConsoleColor.DarkMagenta);
            ColoredPrint.Print($"Our family is {ViewersCount} people now!", ConsoleColor.DarkMagenta);
        }

        public void Unsubscribe(IObserver viewer)
        {
            viewers.Remove(viewer);
            ViewersCount--;
            ColoredPrint.Print($"{viewer} has just left our community.", ConsoleColor.DarkMagenta);
        }

        public void AddArticle(Article article)
        {
            if (article != null)
            {
                ColoredPrint.Print("Adding new article to the blog...", ConsoleColor.DarkMagenta);
                Thread.Sleep(1000);
                ColoredPrint.Print("Notifying all viewers about new article...", ConsoleColor.DarkMagenta);
                Articles.Add(article);
                NotifyViewers(new NotificationData(article.Title, Id, article.Id));
            }
            else
            {
                ColoredPrint.Print("Unpredictable error occured, while trying to add new article to a blog.", ConsoleColor.DarkMagenta);
            }
        }

        private void NotifyViewers(NotificationData data)
        {
            foreach (var viewer in viewers)
            {
                viewer.Notify(data);
            }
        }
    }
}
