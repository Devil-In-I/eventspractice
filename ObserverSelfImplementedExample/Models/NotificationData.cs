﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Models
{
    public class NotificationData
    {
        public int BlogId { get; set; }
        public int ArcticleId { get; set; }
        public string Title { get; set; }

        public NotificationData(string title, int blogId, int arcticleId)
        {
            Title = title ?? "Example data notification";
            BlogId = blogId;
            ArcticleId = arcticleId;
        }
    }
}
