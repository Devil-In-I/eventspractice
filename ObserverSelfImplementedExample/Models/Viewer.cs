﻿using EventsPractice.Utilities;
using ObserverSelfImplementedExample.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverSelfImplementedExample.Models
{
    public class Viewer : IObserver
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public List<NotificationData> ArcticlesToRead { get; set; }

        public Viewer(string username = "1337Abrakadabra228")
        {
            Id = new Random().Next(1, 100);
            Username = username;
            ArcticlesToRead = new List<NotificationData>();
        }
        public void Notify(NotificationData data)
        {
            ArcticlesToRead.Add(data);
            ColoredPrint.Print($"{Username} just recieved notification about new arcticle to read.", ConsoleColor.DarkGreen);
            ColoredPrint.Print($"The article #{data.ArcticleId} is from blog #{data.BlogId}. With title {data.Title}", ConsoleColor.DarkGreen);
        }

        public override string ToString() => $"{Username}";
    }
}
