﻿using ObserverSelfImplementedExample.Models;

var author1 = new Author("Serik");
var author2 = new Author("ZhambylZhabaev");


var viewer1 = new Viewer("UbiycaNubov");
var viewer2 = new Viewer("Stray228");
var viewer3 = new Viewer("Akter");

var blog1 = author1.CreateBlog("Freedom Finance");
var blog2 = author2.CreateBlog("The problems of the ethics of scientists");

blog1.Subscribe(viewer3);
blog1.Subscribe(viewer2);

blog2.Subscribe(viewer1);

author1.CreateNewArticle(new Article("Introduction to investments", "Some 150 lines long very-very smart text."));
author2.CreateNewArticle(new Article("Ancient Greece. Beginning.", "Another truly smart text that can be understood only be Ubermehsch."));

blog1.Unsubscribe(viewer3);

author1.CreateNewArticle(new Article("Funds. Basics.", "Rabota ne walk, rabota eto work. Walk eto hodit'"));

blog2.Unsubscribe(viewer1);

author2.CreateNewArticle(new Article("Another philosophical artical", "You're not yourself when you hungry. Yeat snickers. Snickersni!"));

Console.ReadKey();